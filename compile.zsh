#!/usr/bin/env zsh

for d in src/*; do
	echo d: \"$d\"
	pushd $d
		date=$(basename $d)
		if [[ $date == future ]]; then continue; fi
		pandoc blog.md -o blog.html
		echo dir: $(pwd)
		cp blog.html ../../public/${date}.html
	popd
done
