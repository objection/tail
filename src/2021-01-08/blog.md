# A real programmer

They say a real programmer is lazy. They hate doing things so much
they spend weeks writing tools that will save them minutes. Of course,
sometimes they write tools that will save lots of people centuries.

What I'm getting at is I wrote a script today. It tells you the word
count of each chapter in your story.

I say "chapter", but really I mean --

What do you call it, when you, you know:

> "Blah, blah, blah", said Percy.
>
> Little he know, the fool, that that blah was the last blah he would
> every blah.
>
>
>
> On the following Tuesday I packed up my jimjams and set out into
> the Wild.

You don't call that a chapter. I'll die before I call it a scene (I'm
not a director, and I don't secretly wish I was one).

Well, anyway, my script counts up those. I wrote it in C. Would you
like to _C_?

```C

#include <err.h>
#include "../wc/wc.h"
#include "../cat/cat.h"
#include "../split/split.h"

int main (int argc, char **argv) {

	if (argc < 2) errx (1, "usage: %s file", *argv);

	char *buf;
	int ret = cat_paths_into_buf (&buf,
		(const char *[]) {argv[1]}, 1, 0);

	size_t n_bufs;
	char **bufs = split (buf,
			strchr (buf, '\0'),
			(char *[]) {"##"}, 1, &n_bufs, 1);

	struct wc wc;

	each (buf, &bufs[1], n_bufs - 1) {
		wc = wc_str (*buf);
		printf ("%zu: %zu\n",
				buf - bufs + 1 - 1, wc.words);
	}
}

```

I'll tell you some interesting things about this program.

## 1!!!!!!!!!!!!!!!!!!!!!!! (each)

I like to use a macro to loop over arrays. In C, traditionally you do

```C

for (int i = 0; i < /\* eg \*/ 100;  i++) {
	/\* eg \*/ printf ("%d\n", the_thing[i]) };
}

```

And I think that's more universally useful. I think if you were going
to have just one kind of for loop, that would be it. But I like the
other kind, the kind you get in Bash and no doubt Python and all the
rest: "for THING in THINGS".

The each macro lets me do that. Actually I can do that without a
macro, and did for a long time.

```C

for (struct thing *thing = things;
		thing < things + n_things; thing++) {
	(*thing).whatever = "joy";
}

```

But that's pretty painful to write because it makes your eyes bleed.

## 2!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

I include the header files "wc.h", "cat.h" and "split.h". These are
libs I've written. split.h is straightforward: split a string. cat and
wc, though, might be interesting to talk about.

C's a bad language to write little scripts like this in, or so, I
feel, people believe. But I say screw you.

What makes C bad as scripting language is the standard library. It
doesn't have convenient functions. As an example there no function
simply split a string. There's strtok. It's not really the same.
There's no join, launch_emacs, no nothin. There's actually no dynamic
array struct-plus-function. Maybe because no one would agree on the
best way to do it?

The program I pasted: would it really be that much shorter in Python?
I say NO! Not _that_ much shorter. Not when you have libs like wc,
cat. But you have to write those yourself.

You can use big general-purpose libraries like glib and maybe one day
I will. I've never liked the idea of linking to such a big binary just
to write programs like the one I. But I think that's empty prejudice.
Most scripts you write yourself in Bash are personal. I mean, they run
on _your_ machine, and glib (or whatever) is on _your_ machine.

My wc and cat libs are based, you'll know if you're in the know, on
Unix commands: wc (wordcount) and cat (concatenate -- it reads in
files, outputs them to the screen).

I've thought for a long time I'd love to see GNU's Coreutils designed
as C libraries. The commands would be frontends for those libraries.
Because, like I say, the pain in C isn't the language, it's the
low-level nature of its standard library.

Even other libraries are obnoxiously low-level. There's a
commonly-used library, PCRE (Perl-Compatible Regular Expressions).
It's very clever, well made, has loads of functions.

But it doesn't have a "match" function! I swear, I've looked. There's
no function you can call to just figure out if a bit of text matches a
pattern. Instead you're expected to compile a regex, then execute it,
checking for errors along the way. It about six lines for something
that in in other languages is simply "==" or "=~".

So, of course, the first thing you do when faced with that is wrap it
in your own function, regmatch and forget about all the rest.

What else do I have to complain about?

Nothing. What a disgrace.

That was a pretty incoherent blog post, but I've overdone it on the
coffee / energy drinks and have a headache.

## Oh

I suppose I should say something about the story. I'll tell you I've
come up with a name: The Long, Thin Tail of God. I'll also spurt out
here the names I considered. Fast content, ker-splat, paste it in.

* Home
* Lonely
* Marrowbone War
* Burane Massacre
* Big Rock on a Beach
* Big House on the Hill
* The Whore of Black Lake
* To Jump on My Bed
* Jumping At Every Sound
* An Archaeologist
* A Spy
* It Wasn't Sheer Bliss
* Ice-cold home
* As If I Was A Stranger
* Xunotic
* Xunophillia
* Xunophobia
* Because You're not _worthy_, Soraen
* Right in the Centre of Town
* _Yes_, I Would Have Said
* Keep the Fire Burning
* Eternal Flame
* What a Lot of Work it Must Take
* The Burané Massacre.
* The Ape-People
* The Monkey God
* The Strange Tale of the Long, Thin Tail
* The Long, Thin tail -- that's a nice pun.
* The Long, Thin Tail of God

I went with the Tail one for a very practical reason: the story is, in
the end, about this monkey god, Xu. And there's bugger all about him
in story until near the end. I kind of liked that. I like stories that
surprise me. I don't have strong opinions about how stories should be.

But I know if I don't drop heavy hints about the God before he's
mentioned at the end, people will say, "Huh? Where did _he_ come
from?"

_He came from your mama! Now shut up and just enjoy the damn story!_


Repo: [gitlab](https://gitlab.com/objection/everyone-is-leaving.git)
