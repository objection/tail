# An Announcement

I'll announce my next story. I haven't bothered to send the last one
to anyone yet. Feeling a bit low. Should have written today: didn't.

Programmed. Thought I should do something on my game before it got
stale in my head. The last one got stale.

I suppose I don't regret it, even though progress-wise I've gone
backwards. The game works more poorly than it did. But it's _fine_.
More important right now than writing the game is understanding the
very simple maths behind it. Doing that is just a matter of
familiarity.

I was thinking but a moment ago: I don't learn anything. Maybe no one
does. Or maybe (likely) my definition of "learning" is wrong. I feel
like I should actually understand what the fuck I'm doing while I'm
doing it. But I don't. I gain  _familiarity_ with things.

Take C variable declarations. They're notoriously difficult to parse.
And I can parse them. I'm written/read a million of them. But could I
explain to you how I'm doing it? I can't tell you what a preposition
is, either.

I seem to be so incapable of drilling technical information into my
head. What happens to me is Knowledge drips in while I'm not looking.
Usually I'm asleep.

I guess what I'm complaining about is everything is so fucking hard.
We're not brainboxes, none of us. Experts look superhuman when they're
doing their thing. But they can't instantly transfer that skill to
other things. They built up the skill over _ages_. It's fucking
bullshit; it's a joke.

But the game stuff -- maths stuff -- is dribbling in there. A vector?
OK, I've got that. A unit vector -- OK, I  remember. A normal vector?
Can't remember. Length of a vector? - yeah; dot product, blah, blah. I
have progressed. I'm not falling at the first hurdle any more.

Same goes with writing. Actually, I feel a lot more confident
recently. I can really see each story being better than the last.
That's the bullshit I'm talking about. I've been at this for _years_.
Any sane god would have preinstalled me with a mastery of all things.

I'd be writing novels with my left hand, drinking with my right. God,
I'd be so rich. Surrounded by cats. I'd teach them to write. That
would be a good use of my time.

The next story's gonna be too long. It's a murder mystery. Damn right.
A magical murder mystery. That's a little worrying. It's not a fair
mystery if the reader can't guess who dunnit and how. It's easy to
make that impossible with magic.

The trick will be to properly introduce the magical idea and close off
any others. Meaning: explain the "magic system" (I scare-quote that
just cause the term makes me sick). Problem with explaining magic
systems is it's not proper magic any more, then, is it? Proper magic
is inexplicable. Nothing wrong with explicable magic. Apart from it's
not fucking magical.

The important part of this rambling blog post is I'm writing the
damn story. I came up with the whole thing yesterday, and it's a good
one. Tomorrow I'll get the names and places and shit done. I like
having as much of that ready as possible.

The most important part of the important part is the deadline.
Tick tock. Tick tock. One week for the writing. That's the sixth.

No. Let's be fair about this. I started yesterday, not tomorrow. So
the deadline's next Thursday, the 4th, which I'd call tight. But I
deserve everything I get.

Wait a minute. Let's think about this, slowly, painfully. You can
watch with me the neurons banging together like prehistoric man
starting a fire.

I'm aiming for 7200 words. That's 'cause Stephen Kings says someone
else said 2nd draft = 1st draft - 10%. I've already told you how I
believe the magical word count is 6473.

There's two problems with this. 1) I can see that this is a +7200 word
story; and 2) I never quite manage to shed 10%. The last story I shed
about 1% and think I stripped half the meat from its bones in the
process.

Maybe I will aim for 10,000. I think it'd be a sad story if I didn't.
Let's just say it's 10,000, then. I can write about 1,500 words a day,
so 10,000 / 1,500 is 6.6. A week.

Well, what a terrible thing; looks like I get to finish on the sixth
after all. Not quite a week: I won't be working today.

This time I won't stick the thing up on Gitlab. I removed the links
from the previous posts, took the story down.

And I'll keep writing here. I'll talk about what I've been doing.
