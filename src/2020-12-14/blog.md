# Writing, Markdown and Ctags

## Writing's too hard

For a long time I've tried to make writing stories more like
programming. Programmers have all sorts of tools.

A compiler's a tool, not just because it compiles the program for but
because it finds mistakes for you. Mistakes like You Didn't Initialise
this Variable and That Function Doesn't Exist.

What's the writer's equivalent? I suppose a spell- or grammar-check.
But there's nothing that will make sure your story doesn't have plot
holes. You have to -- why does it hurt so much? -- use your brain.

Programmers also have lots of ways to get to places in their projects
quickly. One tool that does this is
Ctags.

## Ctags

Ctags is a program that makes a sort of database of your code. Editors
like Vim use it to let you jump directly to functions, data
structures, etc. It's really, really, really fucking useful.

Here's an attempt at a demonstration. Say you've written a program
with one function in it, called do\_the\_thing. If you run Ctags on
that file, Ctags will write a tag file that your editor can read
so it knows where that function is. So in Vim, for example, you
could type ":tj do\_the\_thing" and bam, Vim will take you there. You could also
(in Vim) put your cursor somewhere that function is written and press
ctrl + ]. Bam. Like:

```

	Your cursor ↓
		do_the_thing ();

```

Bam.

So I wanted Ctags for writing. But it didn't work on Markdown, and
nobody seemed to care!!!! No fucker, it seemed, shared my longing. I
even tried to write my own program to do it. A few times actually. I
failed.

Well, now Ctags works with Markdown. Universal Ctags, that is. The
version I was using (that's still default in most Linux distros) is
Exuberant Ctags. Universal is a newer fork.

So now I write all of my notes in Markdown. Here's an example:

```markdown

# Soraen
# Soraen Tororo
# our guy

## Personality
	Blunt but charming
	Business-minded and has fucked someone over in the past
	Soft-hearted
	A hoe

## From
	Another town, not far
	That's why he's got no parents

## History

	He left his town because his parents are ice-cold

	He got at job at a jeweler's, dicked over the guy when he was old
			and took over the shop

## Looks
	Good-looking, blonde

## Age
	Forty

```


In Markdown, you define headings with #. You add more #s to the # to
increase the heading level.

You'll see I've made three top-level headings, "Soraen", "Soraen
Tororo" and "our guy". If you were to compile this to HTML or epub
(which is what you do with Markdown) you'd have three headings one
after the other and it'd look silly. I suppose I'm using them sort of
as tags. I want to be able to get to Soraen by typing any of these
things.

In fact, the file I've pasted here isn't really valid Markdown. The
indented stuff would be interpreted as a block quote, I think. But it
hardly matters. I'm not going to compile it to HTML.

Repo: [gitlab](https://gitlab.com/objection/everyone-is-leaving.git)

