# Doing two things at once

One thing I've always struggled with is, frankly, being a lazy fucker.
But, being kinder to myself, let's say it's time-management.

I'd love to be a multitasking operating system. Doing a bit of
this, then a bit of that, the other. My mother always says she's
always doing fifty things at once.

This is true, though some good Starcraft players like to point out
nobody ever does even two things at once. You can't actually macro and
move your army at the same time. It just looks like it when you're
doing it really fast. Same with multitasking operating systems,
putting aside threads.

That's neither here nor there, really; it's just something I've been
thinking about. What I'm saying is when I work, I work and when I fuck
about I fuck about. Two modes, nothing in between.

I try to be more like my mother. Her house is always clean. Always. I
never really thought about it. There's no socks on the floor. Plates
are stuck in the sink, then washed, perhaps immediately, perhaps after
soaking.

Cleaning's an algorithm. Like Bubble Sort. Or, at least it is how I --
and I think she -- does it. Plates found in the living room go
into the sink. Stuff found in the sink gets washed, put on the drying
rack. Stuff on the drying rack gets put in the cupboard.

I swear, this is the kind of shit I think about. And yet, my house is
untidy. That's because of my laziness.

So, that's how I try to approach cleaning. I've been doing pretty
well. I say "cleaning's an algorithm" -- that's how I put it, sitting
here staring at a fullscreen Vim, having made a directory with mkdir.
Mother just says you should do things bit by bit.

It's good for your brain, too. It's nice to KEEP ON TOP OF THINGS.
The alternative, by the way, is drowning.

Anyway, these musings are me working up to an explanation for why my
story's not finished. Actually, it's 8 hours from completion. But I've
had editing jobs this week. One of them's going to pay a lot -- not a
normal scenario for me. So I've been waiting for them to come to me.

I have ever since about fifteen had problems with sleeping. My mother
did, too, till she was about sixty, ie, past the menopause ("the best
thing that ever happened to me"). I can't sleep when I'm expecting
something. I don't mean worrying. I'm not usually worried -- at least,
not anxious; not even, really, thinking about the thing.

But I can't sleep.

I'm saying if I'm waiting, I'm waiting. That's the state. It's like
this C code:

```C

	switch (state) {
		case STATE_EXPECTING:
			while (job_not_here)
				for (int i = 0; i < get_episode_rating (); i++)
					detective_conan ();
			break;
		case STATE_WORKING: {
			int work_secs_left = 8 * SECS_IN_HOUR;
			enum something_elses {
				TOILET, DISHES, HOOVER, N_SOMETHING_ELSES,
			};
			while (secs_left > 0) {
				secs -= work (1 * SECS_IN_HOUR);
				secs -= do_something_else (random %
						N_SOMETHING_ELSES);
				break
			}
		}
	};


```

Probably there's a bug there.

So, I _am_ working. But I should have finished my story. I was meant
to get my piece to edit yesterday but didn't. Since I knew I wouldn't
get it early on I could have finished my story. Instead I wrote a
Syntastic checker plugin for Csound.
