# Let me complain about Meson

## About Hobbits

Meson's a build system. Or possibly Ninja, Meson's backend, is a
build system. Let's just say the whole thing is.

It's an alternative to CMake, which is an alternative to the
Autotools, which is a way to write makefiles that work on various
systems for fewer pints of blood and sweat. And makefiles are
basically recipes, a convenient way to run shell commands that (most
commonly) translate your source code into a binary that your computer
can run.

When I first used Meson I went, "Wow". I was thrilled. I've never got
my head around makefiles, I think because -- well, I'm always saying
I'm stupid, so this time I'll say -- I'm lazy. Makefiles aren't
complicated at their core. I think what's confusing is the many
shortcuts you can take with them. Many shortcuts make things hard to
learn. You can't see the wood for the trees. And cruft.

If I do one day finally learn Make, I'll deliberately restrict myself
to the old, more verbose syntax. This general approach is what
everyone should take whenever they learn something. Start with the
scales (music analogy). I'm a guitarist and I've hardly bothered.
They're boring. And you think ('cause you're arrogant) _other_ people
need to start with the scales, OK -- and there's nothing wrong with
being dumb! -- but me ...

Lots of us are like this. We think we understand enough or at least
trust our perception of our own intelligence -- and rush ahead. When
people ask other people how to make computer games they get told to
make Pong. "Actually, sorry, no," says OP, "I'm making an open-world
simulation CRPG, thanks. Yes, I know it'll take me a while."

You've got to make Pong. I'm thirty-six and I've known this for years.
And yet the game I'm writing is a Zelda clone. However, I _have_ paid
my dues, having written approximately 1,00,000 command-line programs.

The problem with bells-and-whistles, do-everything-for-you _things_
(like Meson) is what do you do when you can't get it to do the thing
you you need it to do? Look in its documentation. Or possibly the
documentation for one of the many programs/libraries/framewords/apis
it uses. And when you do, you find (tenuous metaphor) they're talking
Mario and GTA and the Elder Scrolls and you don't understand, because
you didn't make Pong.

They don't want you to make Pong. "Try our
system/framework/platform. It's got x and y and you'll love it! You'll
never have to bother with all those low-level things again."

Tom Waits can [explain it
better](https://www.youtube.com/watch?v=A2_snSkpULQ).

And you end up dumb as a brick. A user instead of wizard you deserve
to be. They took your magic wand.

So why am I ranting away about this? As any mediocre scriptwriter will
be able to guess, there has been an inciting incident. I am incited,
and an incident is responsible.

Basically, I was playing with Zig [^1]. No one's written a Syntastic
(Vim linting plugin) checker for it, but there is a Zig language
server. So I got rid of Syntastic and got ALE (Asynchronous Language
... ?), which does the same job as Syntastic, but asynchronously and
with LSP support.

LSP -- Language Server Protocol -- is a Microsoft thing. A good thing,
a way for any editor to offer lots of IDE-like things. We could always
do those things, with various tools like Ctags but this does it
better. Because it makes use of your actual compiler or interpreter's
output.

Getting it set up's not easy, though, though it worked for me first
try this time. A testament to how much I've learned? Maybe, but ALE
knew where to look for the compile\_commands.json and Meson stuck it
in the right place.

I always make an effort to properly introduce technical things I talk
about, for the sake of the fictional layperson. I, for one, get bored
and stop listening to things I don't understand. But it's hard, and
I've failed here before even getting to my point.

Which is systems like Meson are shit. I'll keep using it, though. I
won't write Pong, and I won't use Cmake.

The reason it's shit is I've spent five hours trying to silence a
clangd warning. clangd's the name of a language server. For C and C++.
At some point today I completely forgot about Zig.

I use gcc to compile my C programs, and too eagerly use gcc
extensions. The language server stuff is all to do with clang. So,
though I'm _compiling_ with gcc, _clang_ is being used to _LINT_ [^2]
my C program.

This should be fine. clang claims to be a drop-in replacement for gcc.
But it's not.

## Tom Bombadil

I like gcc's "-fms-extensions" flag. That lets you include structs
that have already been defined as anonymous members of another struct.

```C

struct apple {
	char *name;
};

struct orange {
	struct apple;
};

```

It's \-fms-extensions that permits the nameless struct apple inside
the struct orange. Normally you've have to give it a name, like:

```C

struct orange {
	struct apple apple;
};

```

And refer to it like `orange.apple.name = "Frederick"`.
-fms-extensions lets you do `orange.name = "Frederick"`.

It's just _nice_. I'll show you another trick, while I'm on the
subject.

It fixes the only downside of this approach, which is that now you
can't refer to the member struct as itself: it doesn't have name. But!

```C

struct orange {
	union {
		struct apple;
		struct apple apple;
	};
}

```

Now you are eating your cake in addition to having it. You can now
refer to apple's members without saying apple's name. And you can pass
just the apple to functions that expect one. By writing orange.apple.

One last thing on this topic. Even without -fms-extensions you can
mostly do this. You can define anonymous structs, anonymous unions.
You just can't define a struct outside and then use it inside without
its name. You can do this:

```C

struct fruit {
	char *name;
	union {
		struct {
			float sourness;
		}; // oranges
		struct {
			float crunchiness;
		};  // apples
	}
}

```

## The Barrow Downs

Right now I'd rather know the language of Make. I would have silenced
that warning in a jiffy. I'd be rich by now, the time I saved.

It's swings and roundabouts. I'm obsessed  with this idea. There's no
such thing as a free lunch. Programmers (and maybe writers) know this
better than anyone.

You do this really awesome thing in an effort to make your program or
story better. And it takes ages. And when you're done you have ...

Oh, sometimes you'll have more flexible code, or more robust code, or
faster or more memory-efficient code. But you've sacrificed something.
Readability, perhaps. Speed. Whatever. It's gone and there's _no_
fucking way you're going back over it again. You're stuck with it.
You'll defend your decision to the death. You _bled_ for it.

Meson's big selling-point is it just works. Oh, it's worth it. I said
it was shit earlier -- that was a lie. But I'm still mad it took me
ages to fix my linter problem.

## Hey, Wait, We're in Mordor?

I've got a new complaint.

When I first started programming, I used Visual Studio and Windows. I
remember how hard it was for me to compile my first program, which was
probably an SDL example. Probably there was Hello, World before that.

Someone said somewhere the hardest thing you'll ever do in programming
is compile your first program. And, oh, I agree. Because there's all
this stuff to learn.

I buggered off to Linux, partly because I'd come to realise if you
want to program, particularly in C, it was the place to be. A lot of
programming in Windows and Mac is programming in Linux. Users of those
OSes use virtual machines, compatibility layers and servers to do it.
They have, I am sure, mighty brains, because it's one thing to compile
a program, and quite another cross-compile it, or do it in a VM or
container, or do it on the web somehow.

What I didn't like about Visual Studio was simple: you gave the
compiler and linker and build system options by filling out textboxes
and picking from menus.

I roared, "But how does it  _work_?"

I felt strongly that Visual Studio's friendly user interface was
obscuring the reality of what I was doing. Now it blindingly obvious
to me it's turning all those textboxes, checkboxes into a
commandline, which it'll fire at the compiler. But I didn't then.

Meson gives me a strong whiff of that. Look.

```meson

	add_global_arguments ('-fms-extensions', language: 'c')
	add_global_arguments ('-Wno-microsoft', language: 'c')
	m_dep = cc.find_library ('m', required : true)
	sdl2_dep = cc.find_library ('SDL2', required : true)
	sdl2_image_dep = cc.find_library ('SDL2_image', required : true)
	sdl2_ttf_dep = cc.find_library ('SDL2_ttf', required : true)

```

These are just commandline flags. Meson is  taking these strings you
give it -- "SDL2", "-fms-extensions", etc -- and appending it to a
call  to gcc. The cc.find\_library function is calling something like
pkg-config or cmake. Is all this stuff really better than:

```bash

	gcc -ggdb3 -Wall src/* -fms-extensions -Wno-microsoft \
		-o build/whatever -l -lm \
			$(pkg-config sdl2 --cflags --libs)  \
			$(pkg-config sdl2_ttf --cflags --libs) \
			$(pkg-config sdl2_image --cflags --libs) \

```

Maybe so.

In summary, I could have solved this Meson/LSP/ALE/Vim thing in five
seconds flat if I'd written a makefile (or, frankly, since my project
is hardly huge, a shell script). But I won't start writing Makefiles
any time soon. I reserve the right to complain about it in the future,
though.


[^1]: a language I definitely approve of, that's packed good ideas and
  things done right, that I probably won't use, because already know
  how to do the things it tries to solve, and learning new languages
  makes me feel like a toddler or an old man. Maybe one day! But it's
  new, too, and if there's one solid lesson I've learned in my years
  using Linux and programming it's don't use new things. Use old and
  safely dead things, expecially those whose undead life is regulated
  by crusty old men and women. Because there's documentation! And
  they're getting round to implementing those features you envy.
  They'll get there. And in the meantime, well, you can do it  gcc
  already.

[^2]: A linter is a program that looks at your code and points out
  some kind of problem. Some show syntax errors, some tell you that it
  doesn't like your coding style. Some just annoy the shit out of you
  and you don't know how to shut them up.
