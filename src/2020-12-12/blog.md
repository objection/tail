# 6473 Words

## Crap

Wow, I just lost this entry. That's something I've not done in about
five years. It was fucking gold, too. Let's reheat it, see how it
tastes. They eat gold, you know, rich people, with beef. Disgusting
behaviour, isn't it?

## Basically

What I was saying was the story's not going to be ~5,000 words. It's
going to be longer and that sucks.

So I thought I'd go look again Beneath Ceaseless Skies' submission
guidelines. Where did I get this 5,000 words number from, you know?
All they  say is they're looking for < 15,000 words. They say if
you've gone over you better have a good reason.

So what's the average Beneath Ceaseless Skies story length? I found
out.

I already had all of their stories ripped from their website. I'm not
sure if that's naughty or not. I did it by scraping the website with
wget, formatting the html in text with w3m and chopping the non-story
bits with a Vim script.

I'm really proud. And I'm proud of what I did now:

```zsh

cd ~/blah/beneath-ceaseless-skies-stories

for f in */*.txt;
	do wc -l  $f >> /tmp/beneath-ceaseless-skies-wordcounts.txt
done | sort -n

total_words=0;

n_lines=$(wc -l /tmp/beneath-ceaseless-skies-wordcounts.txt | cut -d' '  -f1);
for num in $(cut -d' ' -f1 < /tmp/beneath-ceaseless-skies-wordcounts.txt );
	do total_words=$(( $total_words + $num ));
done;

echo $(( $total_words / $n_lines ));

```

Cool, huh? I won't explain what it does. Have you ever tried to
explain what _anything_ does? It's hard. It's much harder than making
up stories.

I mean, it gets the average. And by average I mean mean. The sum of
the array divided by the number of elements in the array.

Anyway, the answer is 6473 words. A comforting answer. 6473 is a big
enough space to tell a story in and not so big your trousers
fall down.

That sentence was wittier the first time around.

What else did I say? I gave more of a justification for having ripped
Beneath Ceaseless Skies's stories. I mean, it's LEGAL. Right? Yeah,
yeah, yeah. It might become illegal if I distributed it. Of course, it
_is_ naughty. They'd like me to look at their ads. But I use adblock!
So what am I saying?

I guess that I've done nothing wrong and in fact might have made the
world a better place. Certainly I've benefitted myself. What power!
6473 words.

Repo: [gitlab](https://gitlab.com/objection/everyone-is-leaving.git)
