# Fat Cunt, Reformed

In Britain some time ago there was an advert. Can't remember what it
was for. "Belly's gonna get you, belly's gonna get you." I'm
got. My tits are pretty good, frankly.

And so I'm on a diet. I'm taking this quite seriously. I've gone and
looked up BMR (Basal Metabolic Rate) and I'm measuring the weights of
foods, figuring out their calorie content.

I wrote a program to help me (of course) called ccalc. Just a simple
calorie calculator. You go, `ccalc rice tatties "monster energy
drink:500"` and it tells you ... wait a minute ... -- 438.00. The program
reads from a file I'm compiling over time. Looks like this:

```

rice:                           1.29
indomie:                        5.0
tatties:                        .74
onions:                         0.42
mushrooms:                      .22
spaghetti:                      1.57
canned tomatoes:                .17
bread:                          2.66
peppers:                        .26
french fries:                   2.74
ice cream:                      2.01
broccoli:                       .34
coffee:                         .01
milk:                           .60
monster:                        .47
walls cheese and onion slice:   2.96
microwave korma:                1.48
egg korma:                      1.64
quinoa bulgar wheat:            .85
cous cous:                      1.07
chia seeds:                     4.47
eat natural bar:                4.76
sesame oil:                     8.84
olive oil:                      8.84
vegetable oil:                  8.84
banana:                         1.5
peanut butter:                  5.88
almond milk:                    .5
blueberries:                    .83
girlfriend smoothie:            .55


```

The numbers on the right are cals-per-1-gram. I've got them all from
fatsecret.com, so if I'm wrong, blame them. Or my shoddy grasp of even
the simplest mathematics.

Here's some interesting things doing this has taught me.

* Good god, shrooms[^1] have fuck-all calories in them. I love shrooms
  and could eat them all day. Seriously, there were about 150 grams of
  shrooms in the pack I ate today. That's ... uh, sec ... 33.00
  calories! What? How can that possibly be true? Maybe it'll turn out
  to not be true. But I believe it. Shrooms live in the shade. What
  need do they have for calories?

* Oils of all kinds are jam-packed with calories. Even a tablespoon of
  sesame oil is 120 cals. I've been using more and more oil as I march
  statelyly through my life. No more, I guess.

* Energy drinks are really not that bad in terms of calories. Less than
  instant noodles. Of course, instant noodles fill you up. That _is_
  significant. And energy drinks are empty calories. I had an energy
  drink on the first day and got mad later 'cause I was hungry and
  had blown 1/10^th^ (or so) of my calorie budget.

* There's more calories in one of my girlfriend's
  so-healthy-they're-gross green smoothies than there is in an energy
  drink. Yes, yes -- "but they've got nutrients in them". How many
  nutrients do you need? Am I in danger of scurvy? Are you?

* Chips are ... -- no more chips. And when I say "chips" I mean the
  last bit of "fish and chips".

* Onions, broccoli -- all vegetables except potatoes -- are
  low-calorie.

Anyway, I'm having fun. Really. It's nice to sort of manage your life.
A bit like being a dictator, or an abusive spouse. The problem with
being those things is people piss on your grave. But when _your_
hated dictator dies ... you'll be dead, too!

[^1]: I mean, of course, mundane, verynomagic ones (Vim joke).
