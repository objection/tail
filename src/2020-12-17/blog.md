# As good as my word

I'm done. I said I'd do it in a week and I did it. I mucked about two
days, had to rush in the end. But that hardly matters.

I benefitted from a bad mathematical brain, I think. Yesterday I
thought my deadline was that day. So I bashed out about 3,000 words.
That's the most words I've written in one day in years. They were
half-decent words, too.

In contrast, today's been half-assed. I've got to the end, but what
I've got is sketchy. A little undercooked.

I'm often not sure how to end my stories. I often try to do something
subtle, because can _feel_ there's something _big_ behind my (short)
story. But I often can't bring it to light. I'd be better off cutting,
simplifying. And, perhaps I will, here.

Thinking about the story now, I think this might be my best yet once
it's done. It's come from a good idea, it moves along.

It shares a flaw with, I swear, all of my stories, which is it's ...
subtle. What I mean is it might be a loose bunch of ideas barely
hanging together as a story.

Doesn't matter. Doesn't matter. What matters is what I'm going to do
next.

And that's pledge to have the thing edited and done by this time next
week. It's evening, so let's give myself until the 25th.

I've stuck epub and html versions of version 1 into build/v1.

Repo: [gitlab](https://gitlab.com/objection/everyone-is-leaving.git)
