# Day Two

## Note about formatting

I'm not formatting stuff right in these posts, using code tags instead
of, I guess, block quote tags? The colouring is wrong, anyway. Tumblr
thinks it's looking at code.

I also noticed that Tumblr ignored code tags entirely when showing you
posts in your feed.  I guess there's nothing I can do about that.

## Report

A decent success. The story's got something. Let's hope nobody
realises I wrote it around the time series four of Attack on Titan
came out.

It's the weird-things-are-happening type of story. I like that type.
I don't think I've written one, though.

I think I've got it all planned out, nothing missing. I've made sure
to decide exactly why the weird things are happening.

I don't plan to tell the reader, though. I'm going to drop hints. And
I feel confident it's going to work out. Obviously I'm saying I'm
confident because I'm actually nervous -- sometimes people get to the
end of my stories and say, "It's great! I can't wait to read the
rest!"

They don't notice my heart shrivelling up and dying within me.

No, it'll be clear enough that the monkey god is responsible.
There'll be unanswered questions, though, and I hope people won't
flame me for it.

The thing is I _like_ unanswered questions. I don't give a shit about
explanations. You lose something with explanations. Take Donnie Darko,
for example.

That was a great film, of the weird-things-are-happening type. I
bought the Director's Cut DVD and watched its director's commentary.
It had Kevin Smith on it, quizzing the director about his changes,
about what the hell the story was all about.

Well, it turned out the story was about an AI or something in the
future looking back at Donnie and trying to fix some cosmic thing that
had gone wrong. It was a fine reason for what was going on. But it was
better without it.

The more you put into a story the more you narrow what it is. That's
why the best part of a lot of stories is the beginning. And the worst
part us nearly always the end. Oh, there are great endings. But who
cares. I tend to tap out in books about 20% from the end, the bad
guy still not defeated. If it's a part  of a series I get excited for
the next one.

## Doing something different.

I'm doing something different. I'm merging the planning and the
writing stage. Normally what I do is write a synopsis,  put it aside,
write the story. I don't like it,  don't like planning. It feels like
I'm repeating work. And I never look at the synopsis while I'm writing
anyway.

This synopsis is the story, a very abbreviated version. It's written
in rough prose. Normally my synopses look like this.

```

Our guy goes to the place
	Meets whatsherface. She eats him.
		"Why?" he said.
		"Duh," she said.
			Mention that the elephant at the cheese.


```

NOTE: My main character is always "our guy" or "our girl", even  if
she/he's 500 years old.

This one's written in sort-of prose. An excerpt:


```

2

	I didn't search more that day. I was stunned. It was _her_. She
			wasn't _human_.

	Other people started going missing. After a few are gone they
			find them

		They're in a group and they disappear like smoke when people
				find them

		But then more and more

3

	Now it was just me

	I could see them out there, their campfires, small groups, just
			sitting there


```

Right?

The plan is to inflate this like a bicycle tire.

I know other writers work like this. Terry Pratchett called his first
drafts Draft 0. He said it was about 1/3 of the final book. I think
you can see what his Draft 0s looked like if you look at the last
third of his final -- unfinished -- book, The Shepherd's Crown.

I think Brandon Sanderson works kind of like this, too. He's got a
Youtube channel. You should go subscribe. He gives a lot of useful
advice.

If you want to look at the synopsis, it's synopsis.txt in the
repo. I'll leave it there.

## 5,000 words

I'm determined to keep this story within 5,000 words. I find keeping
stories short really hard to do these days. It pisses me off.

You might say, what a great problem to have. But it's  not.  Firstly,
short story magazines don't  want long short  stories from new
writers.  I mean, they'll take  it  if it's incredible. But I don't
know if this is going to be incredible.

Secondly, my stories are getting long because I'm being indulgent.
Short stories are _not_ novels.  I'm  not capable, I think, of writing
novels, so I have to be sensible about it. What I'd like to be next
year is someone who's published, let's say, three short stories. I
don't want to be someone whose published no nothing.

You level up in writing by finishing stories. It's like an RPG. You
don't get XP if you die.

And it's a matter of principle. I used to  pride myself on my ability
to write short stories. I thought I'd really cracked it. I need to
show myself I can still do it.

The last  reason I  have for wanting to keep the story short is I
think letting stories sprawl is a sign of WEAKNESS. I think they
sprawl because you're frightened to solve a problem. It's easier to
defer than do.

Situation: your hero has made it to the Chamber of Death. He needs to
get the Thing of Beauty. But he can't because you had a thief steal it
3,000 words ago. OK then, you say, and have your hero not steal the
Thing.

You have to send him off to the thief's house, beat him up, when you
really should  have just had the thief not steal the thing, or had him
steal the wrong thing, or had him have stolen the thing ages ago and
has now put it back.

Stories are annoying puzzles, the kind where you have to fit shapes
into a small box. I'm saying you should do the work, solve the
actual puzzle rather than make the box bigger.

ALSO. There's something to be said about cramming big things into
small spaces.

Repo: [gitlab](https://gitlab.com/objection/everyone-is-leaving.git)
