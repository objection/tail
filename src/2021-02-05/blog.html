<h1 id="the-cave-of-wonders">The Cave of Wonders</h1>
<p>I wonder if you’re like me? You use Vim, you get bored, you go looking for Vim tricks.</p>
<p>Vim’s like Perl. It’s chockablock with features. A million things you can do and a million ways to do it.</p>
<p>I think it wasn’t always like this. Once (in the mists of time) it was just a text editor with a few commands. Its defining feature was its ability to pipe buffers to programs <a href="#fn1" class="footnote-ref" id="fnref1" role="doc-noteref"><sup>1</sup></a>.</p>
<p>You know, like <code>1,$w !wc -w</code>, which gets you your current buffer’s word count. The “1,$” means use lines 1 to the last. “w” means write. “!” means to a program, not a file. “wc” is a program (word count); “-w” is an option to that program, which means “just print the words” -- normally wc prints words, lines and chars.</p>
<p>Now Vim has a wordcount () function. That makes me sad, nostalgic for a time I never knew, and also happy. I use that function for a straightforward mapping that gets the current file’s wordcount.</p>
<pre><code>nnoremap &lt;leader&gt;w :echo wordcount().words&lt;cr&gt;</code></pre>
<p>I could do it the first way, but this one’s better. It just prints the words. wc also prints the file name – like this: “1350 /home/neil/.vimrc” – which I’d have to chop out.</p>
<p>Now that I think of it, I wouldn’t have had to chop anything if I piped the output of cat to wc: <code>nnoremap &lt;leader&gt;w :1,$w !cat % | wc -w&lt;cr&gt;</code>. The “%” is a Vim thing, not a shell thing. It means “the current file”.</p>
<p>What fun, huh? No, really, this shell/Unix/Linux shit brings me serious, ridiculous joy.</p>
<p>Anyway, I thought I’d show off some of the stuff in my vimrc. There’s priceless treasures inside, I just know it.</p>
<h2 id="remove_qf_item">Remove_QF_item</h2>
<p>The quickfix list is great, the quickfix list is good. But sometimes it’s unwieldy.</p>
<p>What it <em>is</em> is a list you can make in various ways, and scroll through and press enter on entries and jump; to perhaps an entry in the output of the :vimgrep command, perhaps to an error in your program.</p>
<p>Anyway, the unweildy thing is you sometimes end up with too many matches. Your brain explodes and doesn’t clean up after itself.</p>
<p>This function lets you remove quickfix entries. You can’t do that normally, can’t move, copy, delete entries.</p>
<p>I think that you <em>should</em> able to, that you should be able to do all normal mode commands on it. Delete, copy, copy-to-the-end, you know.</p>
<p>What would copying a quickfix item mean, though? God knows. Maybe just copy the lines, maybe copy the lines and addresses so you could … paste them into another quickfix?</p>
<p>I bet you someone could find a use for that. I <em>bet</em> you.</p>
<p>I should say this function isn’t mine. I got off the Internet.</p>
<pre class="vim"><code>
function! RemoveQFItem()
  let curqfidx = line(&#39;.&#39;) - 1
  let qfall = getqflist()
  call remove(qfall, curqfidx)
  call setqflist(qfall, &#39;r&#39;)
  execute curqfidx + 1 . &quot;cfirst&quot;
  :copen
endfunction
:command! RemoveQFItem :call RemoveQFItem()
autocmd FileType qf map &lt;buffer&gt; dd :RemoveQFItem&lt;cr&gt;
</code></pre>
<p>To be perfectly honest, I don’t use this much. A better way is Cfilter.</p>
<h2 id="cfilter">Cfilter</h2>
<p>This is a standard plugin. It comes with Vim, though you have to load it with <code>packadd cfilter</code>. Put said command in your vimrc.</p>
<p>It lets you filter the quickfix list according to a pattern, like this <code>Cfilter /what.\*ever\&gt;/</code>. That one’ll get rid of all entries that don’t match the pattern “what.*ever”.</p>
<p>It’s capital-C-filter because it’s plugin, not a builtin command. In Vim, lowly users are paradoxically relegated to the upper case.</p>
<p>I think it’s most useful form is Cfilter!, which will remove entries that don’t match the pattern. You know. You do a :vimgrep, get way too many matches, say, no, no, I don’t want any matches from the README, do <code>Cfilter! /README/</code>, and smile smugly where no one can see you.</p>
<h2 id="increase-to-textwidth">Increase to textwidth</h2>
<p>Now, <em>this</em> one I’m proud of. I came up with it all by myself.</p>
<p>In Vim, there’s this concept, “textwidth”. It’s the point at which Vim will automatically insert a newline. You’re typing away, and go past – as I actually just did – “textwidth” (which for me in Markdown files is 70) and bam, you’re on a newline. How futuristic.</p>
<p>This isn’t the same as soft line wrapping. Vim users use real newlines, and program in real assembly language on their real PDP-11s.</p>
<p>Anyway, this function will change the size of the window you’re in to its buffer’s textwidth. Let’s say you permit yourself (as I have been) 90 columns for your C files. Well, depending on your font size and the size/resolution of your monitor, you might not be able to see all those columns. Particularly if you’re viewing two buffers vertically split.</p>
<pre><code>        ..........................
        |           |            |
        |           |            |
        |           |            |
        |           |            |
        |           |            |
        |           |            |
        |___________|____________|
</code></pre>
<p>Have no fear. With Increase_to_textwidth, when you move into such a window, it’ll automatically expand horizontally so you can see the whole thing</p>
<pre><code>        ..........................
        |               |        |
        |               |        |
        |               |        |
        |               |        |
        |               |        |
        |               |        |
        |_______________|________|
</code></pre>
<p>Well, it’s not the function that does that. It’s the autocmd that calls it.</p>
<p>Here’s the function.</p>
<pre class="vim"><code>
func! Increase_to_textwidth ()
    if !exists (&quot;g:increase_to_textwidth&quot;)
        return
    elseif g:increase_to_textwidth == 0
        return
    endif
    if exists (&#39;b:increase_to_textwidth&#39;)
        if b:increase_to_textwidth == 0
            return
        endif
    endif
    let b:increase_to_textwidth = 1
    if &amp;tw == 0
        return
    endif

    let width = getwininfo (win_getid ())[0].width

    if width &lt; &amp;tw
        execute &#39;vertical resize&#39; &amp;tw + 2
    endif
endfunc
</code></pre>
<p>(Looks a bit sloppy.)</p>
<p>And here’s the autocmd.</p>
<pre class="vim"><code>
au WinEnter,VimResized *
    \ call Increase_to_textwidth ()
</code></pre>
<p>Autocmds are the real magic in Vim. They’re what let you warp it into your own diabolical creation.</p>
<h2 id="win_operator">Win_operator</h2>
<p>Here’s another one I like, that I did my own very self.</p>
<pre class="vim"><code>
&quot; Not really an operator
func! Win_operator (key)
    let start_win = winnr ()
    execute &#39;wincmd&#39; a:key
    if winnr () != start_win
        wincmd c
    endif
    call win_gotoid (start_win)
endfunc

silent nnoremap &lt;c-w&gt;ch :call Win_operator (&#39;h&#39;)&lt;cr&gt;
silent nnoremap &lt;c-w&gt;cl :call Win_operator (&#39;l&#39;)&lt;cr&gt;
silent nnoremap &lt;c-w&gt;cj :call Win_operator (&#39;j&#39;)&lt;cr&gt;
silent nnoremap &lt;c-w&gt;ck :call Win_operator (&#39;k&#39;)&lt;cr&gt;
</code></pre>
<p>The whole thing, including the mappings, lets you get rid of a window to the top, bottom, left and right of you. I use this all the time.</p>
<p>Does it really save too much time? No, it’s not so different from moving to the window and closing it.</p>
<p>But I always want to do everything from <em>right where I am</em>. Abracadabra.</p>
<h2 id="i-guess-thats-it">I guess that’s it</h2>
<p>Is that it? I really thought there were treasures here. I thought the Cave of Wonders was going to close on us in fire and fury. The textwidth one is good. And the win-operator one. You can’t tell me otherwise.</p>
<p>There’s another one, jump-by-win, which I made into a plugin, but frankly, it broke. So that’s that.</p>
<p>As for the story. Just never mind everything I said about deadlines. I’ve had editing work to do. And, of course, I fucked around for a day. And – of course, of course – the story’s too big. It’s not a 6,473 words one. It’s probably gonna be more than 10,000.</p>
<p>And that worries me. It’s not the time it’ll take. I’ve said I’m doing 12 stories this year, and there’s plenty of time left in February. I’m just worried it’s more than I can chew.</p>
<p>After I got my first and only book published, I was meant to write a novel. I may have mentioned this. And I had no idea what I was doing and the whole thing went tits-up.</p>
<p>I’ve been frightened of long stories ever since. Like men are of marriage.</p>
<p>But no. But no. I do feel pretty confident, as I think I’ve said three times on this blog. The story’s got something. A couple of characters I made up years ago, who I like, and haven’t done anything with for FEAR of fucking them up.</p>
<p>I’m done with such fears, I swear to fucking God! You hear me? I’m done! I’m not afraid any more!</p>
<section class="footnotes" role="doc-endnotes">
<hr />
<ol>
<li id="fn1" role="doc-endnote"><p>Aside from the “modal editor” thing.<a href="#fnref1" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
</ol>
</section>
