# The Cave of Wonders

I wonder if you're like me? You use Vim, you get bored, you go looking
for Vim tricks.

Vim's like Perl. It's chockablock with features. A million things
you can do and a million ways to do it.

I think it wasn't always like this. Once (in the mists of time) it was
just a text editor with a few commands. Its defining feature was its
ability to pipe buffers to programs [^1].

You know, like `1,$w !wc -w`, which gets you your current buffer's
word count. The "1,$" means use lines 1 to the last. "w" means write.
"!" means to a program, not a file. "wc" is a program (word count);
"-w" is an option to that program, which means "just print the words"
\-\- normally wc prints words, lines and chars.

Now Vim has a wordcount () function. That makes me sad, nostalgic for
a time I never knew, and also happy. I use that function for a
straightforward mapping that gets the current file's wordcount.

	nnoremap <leader>w :echo wordcount().words<cr>

I could do it the first way, but this one's better. It just prints
the words. wc also prints the file name -- like this: "1350
/home/neil/.vimrc" -- which I'd have to chop out.

Now that I think of it, I wouldn't have had to chop anything if I
piped the output of cat to wc: `nnoremap <leader>w :1,$w !cat % | wc
-w<cr>`. The "%" is a Vim thing, not a shell thing. It means "the
current file".

What fun, huh? No, really, this shell/Unix/Linux shit brings me
serious, ridiculous joy.

Anyway, I thought I'd show off some of the stuff in my vimrc. There's
priceless treasures inside, I just know it.

## Remove\_QF\_item

The quickfix list is great, the quickfix list is good. But sometimes
it's unwieldy.

What it _is_ is a list you can make in various ways, and scroll
through and press enter on entries and jump; to perhaps an entry
in the output of the :vimgrep command, perhaps to an error in your
program.

Anyway, the unweildy thing is you sometimes end up with too many
matches. Your brain explodes and doesn't clean up after itself.

This function lets you remove quickfix entries. You can't do that
normally, can't move, copy, delete entries.

I think that you _should_ able to, that you should be able
to do all normal mode commands on it. Delete, copy, copy-to-the-end,
you know.

What would copying a quickfix item mean, though? God knows. Maybe just
copy the lines, maybe copy the lines and addresses so you could ...
paste them into another quickfix?

I bet you someone could find a use for that. I _bet_ you.

I should say this function isn't mine. I got off the Internet.

```vim

function! RemoveQFItem()
  let curqfidx = line('.') - 1
  let qfall = getqflist()
  call remove(qfall, curqfidx)
  call setqflist(qfall, 'r')
  execute curqfidx + 1 . "cfirst"
  :copen
endfunction
:command! RemoveQFItem :call RemoveQFItem()
autocmd FileType qf map <buffer> dd :RemoveQFItem<cr>

```

To be perfectly honest, I don't use this much. A better way is
Cfilter.

## Cfilter

This is a standard plugin. It comes with Vim, though you have to load
it with `packadd cfilter`. Put said command in your vimrc.

It lets you filter the quickfix list according to a pattern, like this
`Cfilter /what.\*ever\>/`. That one'll get rid of all entries that
don't match the pattern "what.\*ever".

It's capital-C-filter because it's plugin, not a builtin command. In
Vim, lowly users are paradoxically relegated to the upper case.

I think it's most useful form is Cfilter!, which will remove entries
that don't match the pattern. You know. You do a :vimgrep, get way too
many matches, say, no, no, I don't want any matches from the README,
do `Cfilter! /README/`, and smile smugly where no one can see you.

## Increase to textwidth

Now, _this_ one I'm proud of. I came up with it all by myself.

In Vim, there's this concept, "textwidth". It's the point at which Vim
will automatically insert a newline. You're typing away, and go past
-- as I actually just did -- "textwidth" (which for me in Markdown
files is 70) and bam, you're on a newline. How futuristic.

This isn't the same as soft line wrapping. Vim users use real
newlines, and program in real assembly language on their real PDP-11s.

Anyway, this function will change the size of the window you're in to
its buffer's textwidth. Let's say you permit yourself (as I have been)
90 columns for your C files. Well, depending on your font size and the
size/resolution of your monitor, you might not be able to see all
those columns. Particularly if you're viewing two buffers vertically
split.

```
        ..........................
        |           |            |
        |           |            |
        |           |            |
        |           |            |
        |           |            |
        |           |            |
        |___________|____________|

```

Have no fear. With Increase\_to\_textwidth, when you move into such a
window, it'll automatically expand horizontally so you can see the
whole thing


```
        ..........................
        |               |        |
        |               |        |
        |               |        |
        |               |        |
        |               |        |
        |               |        |
        |_______________|________|

```

Well, it's not the function that does that. It's the autocmd that
calls it.

Here's the function.

```vim

func! Increase_to_textwidth ()
	if !exists ("g:increase_to_textwidth")
		return
	elseif g:increase_to_textwidth == 0
		return
	endif
	if exists ('b:increase_to_textwidth')
		if b:increase_to_textwidth == 0
			return
		endif
	endif
	let b:increase_to_textwidth = 1
	if &tw == 0
		return
	endif

	let width = getwininfo (win_getid ())[0].width

	if width < &tw
		execute 'vertical resize' &tw + 2
	endif
endfunc

```

(Looks a bit sloppy.)

And here's the autocmd.

```vim

au WinEnter,VimResized *
	\ call Increase_to_textwidth ()

```

Autocmds are the real magic in Vim. They're what let you warp it into
your own diabolical creation.

## Win\_operator

Here's another one I like, that I did my own very self.

```vim

" Not really an operator
func! Win_operator (key)
	let start_win = winnr ()
	execute 'wincmd' a:key
	if winnr () != start_win
		wincmd c
	endif
	call win_gotoid (start_win)
endfunc

silent nnoremap <c-w>ch :call Win_operator ('h')<cr>
silent nnoremap <c-w>cl :call Win_operator ('l')<cr>
silent nnoremap <c-w>cj :call Win_operator ('j')<cr>
silent nnoremap <c-w>ck :call Win_operator ('k')<cr>

```

The whole thing, including the mappings, lets you get rid of a window
to the top, bottom, left and right of you. I use this all the time.

Does it really save too much time? No, it's not so different from
moving to the window and closing it.

But I always want to do everything from _right where I am_.
Abracadabra.

## I guess that's it

Is that it? I really thought there were treasures here. I thought the
Cave of Wonders was going to close on us in fire and fury. The
textwidth one is good. And the win-operator one. You can't tell me
otherwise.

There's another one, jump-by-win, which I made into a plugin, but
frankly, it broke. So that's that.

As for the story. Just never mind everything I said about deadlines.
I've had editing work to do. And, of course, I fucked around for a
day. And -- of course, of course -- the story's too big. It's not a
6,473 words one. It's probably gonna be more than 10,000.

And that worries me. It's not the time it'll take. I've said I'm doing
12 stories this year, and there's plenty of time left in February. I'm
just worried it's more than I can chew.

After I got my first and only book published, I was meant to write a
novel. I may have mentioned this. And I had no idea what I was doing
and the whole thing went tits-up.

I've been frightened of long stories ever since. Like men are of
marriage.

But no. But no. I do feel pretty confident, as I think I've said three
times on this blog. The story's got something. A couple of characters
I made up years ago, who I like, and haven't done anything with for
FEAR of fucking them up.

I'm done with such fears, I swear to fucking God! You hear me?
I'm done! I'm not afraid any more!

[^1]: Aside from the "modal editor" thing.
