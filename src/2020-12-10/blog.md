# SWORD

I'm a writer, a cliche. When I was young writing was easy. Now it's
hard. How?

Well, you get scared, don't you? And too ambitious. Your mouth's bigger than
your stomach, your arms are puny.

And you're not at school any more. That's the tricky part. There's
no Sword of Damocles hanging over your head. You need one, or you sit
around wasting your one and only life. It's insane, but that's how it
is.

So let me organise one. I, \*\*\*\* \*\*\*\*\* do pledge to have
written this new story I've come up with before seven days have
passed. It's 10 Dec right now, so let's say I get it done by the end
of 17 Dec, next Thursday.

I'll say a bit more about my situation. I've been published -- a
small publisher, the book sank. I mean, I think people bought it. But
I never saw any of the money. I'm not sure to this day if I got
scammed.

That was quite some time ago. I was contracted to write a follow-up,
but terrible things happened, I went mad, I had no idea what I was
doing, I slinked away into the shadows. As I say, I'm not sure to this
day if I got scammed. I'm also not sure if scammed my publisher. I've
kept my head down since.

It's been quite some time since then. What have I done? Well, not
much, right? I did nothing for many years. In the past couple of years
I've done some writing, finished one story, nearly finished a bunch of
others. I mean, I've always got to the end of the first draft. I've
retired from the race every time somewhere about the middle of the
second.

Some of these stories are ... not bad. The truth is, though, they're
not amazing. They're not what someone of my ego would expect from
someone who was published for the first time as early as I was.

I HAVE BEEN HUMBLED. God knows I needed it. You're not as good as you
think you are. Someone on Youtube a while ago said something like the
tricky thing is we think we're great because we know we have great
taste. We read books and go, "I'd do that part better." "I hate how
she does this. God, is she _stupid_?"

It's like watching streamers on Twitch. They are in fact better than
their audience, but their audience goes, "Why did he get
supply-blocked there?" It's cause he was doing a bunch of things you
don't have the eye to notice, and he's 2000 MMR higher than you, shut
up.

But I am getting better. I can see improvement in all these stories.
Each one is mostly better than the last. Sometimes the ideas are
worse, but they're still better-executed.

I just need to do more. I'm not young. It's a bit scary, to have
considered yourself a writer all your life but not to have really
practiced.

So that's that; there's my pledge. I'm gonna post a diary entry every
day, too. Short. I won't go on and on. I like talking, you know. I
like writing. But I have no writer friends to talk to. I don't think
you need them. Talking does help, though. Some of my best story ideas
come from my talking to myself, explaining to imaginary journalists
how I made my millions.

I'm putting the source on
[Gitlab](https://gitlab.com/objection/everyone-is-leaving.git). If you
know about Git, you'll know you'll be able to go look at all of the
different revisions. You might find it fun. I've thought about
streaming on Twitch, too, so you can watch every word, every
hesitaiton. But thinking about it today made me nervous, so, no, I
won't bother.

This secret blog is about enough for me. It's public, but not really,
cause it's unknown. Pretty perfect.

Here follows the first day of notes. They actually written in my
diary. I won't post notes like this again. It'll all be in the repo.

I should also say I don't plan to be very comprehensible here. I don't
plan to be a blogger, though right now in the throws of enthusiasm it
seems to me doing this (pledge, blog) for every story is a very
intelligent thing to do. But  it's a means to an end. The end, by the
way, is riches, fame, an editor. I'd give my right arm for an editor.

# FIRST DAY

2020-12-10T06:26:36+00:00

I'm not going back to mogen. No. One day I will. Let's do something
new. Yes. 5,000 words for real. I've had plenty of ideas. As usual
I'm influenced by things I've been reading.

Something I've always loved is spies. Really I mean "darkfriends". In
the Wheel of Time, there's darkfriends -- they follow Shai'tan. Anyone
could be one. It was the thing that made WoT really good. A Song of
Ice and Fire has the same thing -- people aren't "darkfriends", but
they can betray you. There's always danger.

Though in a short story I don't think I can do that. I could write a
story about a guy who's found a spy. Or a guy who's a spy himself.
Though that would involve coming up with a palace/court. But I
wouldn't have to go into detail. Do I have one already? Not really.

There's also the shogun's wife idea. A story about a shogun's wife
trying to keep him occupied, save him / the country in some way.

Since it's a short story it could be about her trying to get his
mistress back. She's leaving because he's unstable. But the wife needs
her back because she keeps him stable.

A story about a spy who needs to get some papers for his country.
That'd need a few more thousand words to be worth it.

A dangerous world. That's another idea I've been having. I've written
it down. Worlds with danger -- monsters, weather -- in them are the
best. Middle Earth is dangerous. Randland (WoT's world) is dangerous
(because of darkfriends). The Shadow of the Echo of All That is Lost's
(James Islington's trilogy, not its real name)
world is dangerous -- it's full of ancient, dangerous powers.

Another idea I've been having is about a magic that's only "items".
Only objects of power, no power that a person can themselves wield.
But I've always liked that, fuck knows why.

A story about a magic artefact. That sums up 90% of fantasy.

A fun story about a fun girl.

Which of these do I want to write? I'll look briefly through my story
ideas first.

OK, something important: I've made a note to write a story about a
character that wants something, needs something else and cant have
either. This is how Brandon Sanderson writes. I will make sure to do
that.

Another pertinent note: it says to think of the "perfect" world,
thinking of all the books, games and films I've read/played/watched.

Middle Earth: loneliness, emptiness. Xenoblade Chronicles: a world on
a God's body. Final Fantasy VII: Midgar, black pipes. Final Fantasy
VI: cold waste, mechs. The Shadow of the Whatever: an ancient place
filled with dormant, dangerous things. WoT: the atmosphere of the
first and second novels. The world isn't amazing, come to think about
it.

CJ Cherryh's Alliance Universe: it's sci-fi, but there's a lovely
tenuous feel. Everyone's vulnerable going through space, everyone's
weak.

OK, let's think about that. There's a new season of Attack of Titan.
That's got an incredible premise. I could do something  similar.

Rather than huge monsters, let's have small beautiful people. Who are
kind and intelligent. That's scary because you're the bad guy.

You're an ancient monster. You're perverted, you love fear and pain.
Or maybe you're a human. And perverted -- actually perverted but also
basically normal. But the "monsters" are these beautiful creatures.
They're elven. And they have no respect for you. They'll lock you up
or kill you if they find you.

```

	You WANT:

		To be accepted
			You want to be one of them
				You've found a magic spell that can make you into
						one of them
		To escape
		To kill them all, start the human rebellion
		To escape with
			A magic artifact
				Something that could kill them all
				A weapon that could be used to wage war
				A portal or ship that could take you to the moon
			Your
				mother
				sister
				girlfriend
				father

	The monsters are:

		Humans that changed themselves
			Using a spell
			They gave themselves magic powers
			They look just like you, so you can't tell who's who at
					a glance
				You can tell pretty quickly, though

```

It seems ... OK. What the opposite  of perfect monsters? Imperfect
humans. Maybe it's our guy who is perfect. No, I wouldn't know how to
write that.

Let's just try out a lot of these wants and needs.

```

	He WANTS:
		To be accepted.

	He NEEDS:
		To live happily, discrimination-free?

```

So, he wants to be one of them, but needs to just be accepted. That's
fine.

How does he go about getting what he wants? How do you become someone
else? He could use the same magic spell they did. He could use a
different one. They somehow do it the real way, he the fake way. A bit
like Gattaca.

Let's make the community small. It's not a world, just a village.
An interesting village, and this will have something to do with what
changed the rest of them.

Well, first, how about people  start disappearing and appearing
outside? It's a bit Attack on Titan. And not that original. Really
cool, though.

But that wouldn't come into it. That would have already happened. And
maybe wouldn't be explained. So, they became the superior humans by
this mysterious mechanism. He's trying to fake it.

They all left and seemed to become really happy/intelligent/healthy.
But they won't come back into the city.

They keep disappearing. Are they being taken? I think yes. Finally our
guy is the only one left in the town. And he leaves, tries to become
one of them.

They look different. They're covered in hair. So he shaves all the
dogs and cats in the city, covers himself in their hair and leaves.

He finds a small camp of them. They don't accept him.

Or they do, though they know he's not one of them. I mean, they don't
care. Or maybe they do. Maybe whenever he gets close they leave. Or
maybe they just accept him. Do what you want, they say. They don't run
away. But they don't really talk to him.

If they run away, that'd be cool. Nasty. Lonely for him. If not, you'd
be able to see what they say. Maybe it's only when he covers himself
in fur they let him stay. But they know he's not one of them.

He'd need to figure out what happened to them. In the end he doesn't
do it in time and they all leave.

Or maybe he does figure it out and they leave. He just doesn't get
something.

So what is it?

```

	A drug
	A book
	A religion
	A small sphere
		They keep going to the sphere, standing around, then leaving
		When he finds it, he stands around
		He doesn't feel anything, but he changes to look like them
		The next time he sees them, they're walking -- not as a group
		He follows, tries to talk to them and they don't say anything.

	A minstrel

```

That's a nice mystery. It's got a nice feel to it. Btw I just took a
small detour -- 2020-12-10T08:17:37+00:00 -- to make rsn's random
number not be based on the time.

Randomness in programs. You start by "seeding" a random number
generator. The classic way to do this is "srand (time (NULL))". srand
takes an integer. time (NULL) returns the current second (since the
start of Unix time).

The problem with with this approach is if you run the program more
than once in a second you get the same result. Because the seed is the
same.

Now I'm reading from /dev/random:

```c

	int fd = open ("/dev/random", 0);
	int seed;
	if (fd != -1) {
		ssize_t ret = read (fd, &seed, sizeof seed);
		if (ret != sizeof seed) {
			/* warn ("Couldn't read from /dev/random: using time
				(NULL) instead"); */
			seed = time (NULL);
		}
	} else seed = time (NULL);
	srandom (seed);
	if (fd != -1 && close (fd)) err (1, "Couldn't close /dev/random");

```

You will see it defaults to time (NULL) if for some reason /dev/random
can't be opened.

/dev/random is a psuedo device. Linux "collects" entropy over time and
sticks it in a kind of pool. I have no idea how that works. But you
can read random numbers from /dev/random.

Back to the story. It's a nice mystery, one of those what's-going-on
stories. Do I explain what's going on? Stephen King says that stories
that don't show the monster are cheating. But when you do show the
monster you lose something.

Let's just come up with a half-explanation. Like a book, drug or
sphere. But say nothing else.

Maybe instead of them leaving they go back to the village and don't
let him back in. "Get the fuck out! And _stay out_!" Or they just acts
as if nothing's happened.

Maybe it's him that's put them out there. Magic.

How could he be responsible? It'd be hard for it to be him, if
they're supposed to be content.

Maybe they're experiencing something  _more_. They're actually logged
into the magical multiverse net.

Maybe he spoils it in some way. And they don't get to ascend.

The important thing is what's the thing that's brought them out there.

It's got to be something weird or incredible. Let's favour weird.
Let's get some random sentences:

```

	Its a garbed jollified
	Its a refractable corrugate
	Its a remindful hiccupping
	Its a backboneless chum
	Its a upland erase
	Its a cacuminal compart
	Its a accented bishoping
	Its a emendatory deceive
	Its a isoperimetrical reground
	Its a agrostological appear
	Its a urticant hinnied
	Its a reunionistic presses
	Its a homodont barber
	Its a unscrupulous cellars
	Its a psychic harp
	Its a Laodicean prevaricate
	Its a clipped hurries
	Its a quartic worst
	Its a stimulant eulogised
	Its a tangiest processes
	Its a numerable dosing
	Its a cornered totalizes
	Its a raggle-taggle focussed
	Its a sacrificial dissuade
	Its a synoicous enable
	Its a kingly recapped
	Its a unconscionable woofs
	Its a silver depone
	Its a self-confessed heathenized
	Its a led flocks
	Its a microseismical cons
	Its a hypodermal rehangs
	Its a fogless trephined
	Its a urbanized mutating
	Its a bewitching trauchling
	Its a boozy untruss
	Its a unanxious blow-dries
	Its a elenctic platinise
	Its a usurpative alleviates
	Its a unsatisfiable overbids
	Its a Jugoslav embedded
	Its a redder booby-trap
	Its a torn rinsing
	Its a intelligible fans
	Its a decorative restrict
	Its a annalistic bandying
	Its a oral shikars
	Its a sejant outlast
	Its a sapindaceous mussitate
	Its a evoked antiquates
	Its a secular impaled

```

Of those I like:

```

	Its a psychic harp
	Its a accented bishoping
	Its a silver depone
	Its a backboneless chum
	Its a refractable corrugate
	Its a secular impaled

```

A psychic harp. That's straightforward. It'd be just sitting out
there, playing itself sometimes. Or is someone playing it? Our guy
thinks about going up there, touching the empty space. That's scary.

The accented bishoping -- an accented bishop. Just a bishop,
sitting out there on the hills. Not that great.

Its a silver depone. I'm not sure what a depone is. But it doesn't
matter. I've settled on the psychic harp.

It's psychic. That means it can hear your thoughts. It can
communicate, though, but only in music. Is it communicating, our guy
thinks?

It could play folk songs. Our guy would know the words. But he'd know
various words and wouldn't get far.

Maybe the crux of the story is the meaning of the tune it's playing.
Is it "My Johnny Went off to War" or "The Silver Plate", which  is
about a woman who kills herself because her Johnny cheats on her?

Or maybe the songs are about the town, or God. Let's just say he
listens and changes. He thinks about marching to war. And then
everyone leaves.

Probably best to not bother with the words stuff. Won't be able to
make it work. It's just a mesmerising tune, and it changes him.

And then people leave. This is fine. Will people think it's just
weird / feel unsatisfied? I can't know.

I'm feeling a little self-conscious now because I'm thinking I'll go
post this on my blog, announce that I'm going to finish this story in
a week, put the repo on Gitlab, stream this on Twitch. I like the
idea of it. Probably best not to bother with Twitch.

Alright, let's plan that. Where to post? I can stick it on my Tumblr.
Toks has made a website, apparently. I could do the same. I won't,
though.

```

	Today
		Make repo, stick it on Gitlab.
		Write post for Tumblr, include these notes.
		Write
			All characters, names, etc -- temp names -- wants, needs
			Synopsis
		Clean kitchen
		Call Mother
		Put money on phone
		Read

```

